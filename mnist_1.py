import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data


# Steps :-
# input
# hidden layers wieght & activation
# cost function
# optimizer
# backprop
# no of cycles / epochs

mnist = input_data.read_data_sets("mnist", one_hot=True)

n_nodes_hl1 = 500
n_nodes_hl2 = 500
n_nodes_hl3 = 500

n_classes = 10
mini_batch = 100

x = tf.placeholder(tf.float32, [None, 784])
y = tf.placeholder(tf.float32, [None, n_classes])

def neural_network_model(data):
    hidden_1_layer = {'wieghts': tf.Variable(tf.random_normal([784, n_nodes_hl1])), 'biases': tf.Variable(tf.random_normal([n_nodes_hl1]))}
    hidden_2_layer = {'wieghts': tf.Variable(tf.random_normal([n_nodes_hl1, n_nodes_hl2])), 'biases': tf.Variable(tf.random_normal([n_nodes_hl2]))}
    hidden_3_layer = {'wieghts': tf.Variable(tf.random_normal([n_nodes_hl2, n_nodes_hl3])), 'biases': tf.Variable(tf.random_normal([n_nodes_hl3]))}
    output_layer = {'wieghts': tf.Variable(tf.random_normal([n_nodes_hl3, n_classes])), 'biases': tf.Variable(tf.random_normal([n_classes]))}

    l1 = tf.add(tf.matmul(data, hidden_1_layer['wieghts']), hidden_1_layer['biases'])
    a1 = tf.nn.relu(l1)
    l2 = tf.add(tf.matmul(a1, hidden_2_layer['wieghts']), hidden_2_layer['biases'])
    a2 = tf.nn.relu(l2)
    l3 = tf.add(tf.matmul(a2, hidden_3_layer['wieghts']), hidden_3_layer['biases'])
    a3 = tf.nn.relu(l3)

    output = tf.add(tf.matmul(a3, output_layer['wieghts']), output_layer['biases'])

    return output

def training_the_nn(x, y):
    prediction = neural_network_model(x)
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y, logits=prediction))
    optimizer = tf.train.AdamOptimizer(learning_rate=0.01).minimize(cost)

    hm_epochs = 28

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())

        for epochs in range(hm_epochs):
            epochs_loss = 0
            for _ in range(int(mnist.train.num_examples/mini_batch)):
                epx, epy = mnist.train.next_batch(mini_batch)
                _, c = sess.run([optimizer, cost], feed_dict={x: epx, y: epy})
                epochs_loss += c
            print('epochs', (epochs + 1), 'out of', hm_epochs, 'loss:', epochs_loss)

        correct = tf.equal(tf.argmax(prediction, 1), tf.argmax(y, 1))
        accurecy = tf.reduce_mean(tf.cast(correct, tf.float32))
        print('Accurecy', accurecy.eval({x: mnist.test.images, y: mnist.test.labels}))


training_the_nn(x, y)

